package co.acjs.polls;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.support.v4.app.NavUtils;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

public class Question extends Activity {
	int which, quest, y, n, Tot;
	public SharedPreferences mPrefs;
	public static SharedPreferences.Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_question);
		mPrefs = getSharedPreferences("Poll", MODE_PRIVATE);
		// Show the Up button in the action bar.
		setupActionBar();
		Intent i = getIntent();
		which = i.getIntExtra("which", 0);
		quest = i.getIntExtra("quest", 0);
		TextView q = (TextView) findViewById(R.id.textView1);
		final TextView a = (TextView) findViewById(R.id.textView3);
		switch (which) {
		case 1:
			switch (quest) {
			case 1:
				q.setText(getResources().getString(R.string.qest1_b));
				y = mPrefs.getInt("b_1_y", 0);
				n = mPrefs.getInt("b_1_n", 0);
				break;
			case 2:
				q.setText(getResources().getString(R.string.qest2_b));
				y = mPrefs.getInt("b_2_y", 0);
				n = mPrefs.getInt("b_2_n", 0);
				break;
			case 3:
				q.setText(getResources().getString(R.string.qest3_b));
				y = mPrefs.getInt("b_3_y", 0);
				n = mPrefs.getInt("b_3_n", 0);
				break;
			case 4:
				q.setText(getResources().getString(R.string.qest4_b));
				y = mPrefs.getInt("b_4_y", 0);
				n = mPrefs.getInt("b_4_n", 0);
				break;
			}
			break;
		case 2:
			switch (quest) {
			case 1:
				q.setText(getResources().getString(R.string.qest1_c));
				y = mPrefs.getInt("c_1_y", 0);
				n = mPrefs.getInt("c_1_n", 0);
				break;
			case 2:
				q.setText(getResources().getString(R.string.qest2_c));
				y = mPrefs.getInt("c_2_y", 0);
				n = mPrefs.getInt("c_2_n", 0);
				break;
			case 3:
				q.setText(getResources().getString(R.string.qest3_c));
				y = mPrefs.getInt("c_3_y", 0);
				n = mPrefs.getInt("c_3_n", 0);
				break;
			case 4:
				q.setText(getResources().getString(R.string.qest4_c));
				y = mPrefs.getInt("c_4_y", 0);
				n = mPrefs.getInt("c_4_n", 0);
				break;
			}
			break;
		}
		Tot = y + n;
		a.setText("Y = " + y + "/" + Tot + "N = " + n + "/" + Tot);
		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup1);
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				editor = mPrefs.edit();
				switch (which) {
				case 1:
					switch (quest) {
					case 1:
						y = mPrefs.getInt("b_1_y", 0);
						n = mPrefs.getInt("b_1_n", 0);
						switch (checkedId) {
						case R.id.radio0:
							editor.putInt("b_1_y", y + 1);
							break;
						case R.id.radio1:
							editor.putInt("b_1_n", n + 1);
							break;
						}
						break;
					case 2:
						y = mPrefs.getInt("b_2_y", 0);
						n = mPrefs.getInt("b_2_n", 0);
						switch (checkedId) {
						case R.id.radio0:
							editor.putInt("b_2_y", y + 1);
							break;
						case R.id.radio1:
							editor.putInt("b_2_n", n + 1);
							break;
						}
						break;
					case 3:
						y = mPrefs.getInt("b_3_y", 0);
						n = mPrefs.getInt("b_3_n", 0);
						switch (checkedId) {
						case R.id.radio0:
							editor.putInt("b_3_y", y + 1);
							break;
						case R.id.radio1:
							editor.putInt("b_3_n", n + 1);
							break;
						}
						break;
					case 4:
						y = mPrefs.getInt("b_4_y", 0);
						n = mPrefs.getInt("b_4_n", 0);
						switch (checkedId) {
						case R.id.radio0:
							editor.putInt("b_4_y", y + 1);
							break;
						case R.id.radio1:
							editor.putInt("b_4_n", n + 1);
							break;
						}
						break;
					}
					break;
				case 2:
					switch (quest) {
					case 1:
						y = mPrefs.getInt("c_1_y", 0);
						n = mPrefs.getInt("c_1_n", 0);
						switch (checkedId) {
						case R.id.radio0:
							editor.putInt("c_1_y", y + 1);
							break;
						case R.id.radio1:
							editor.putInt("c_1_n", n + 1);
							break;
						}
						break;
					case 2:
						y = mPrefs.getInt("c_2_y", 0);
						n = mPrefs.getInt("c_2_n", 0);
						switch (checkedId) {
						case R.id.radio0:
							editor.putInt("c_2_y", y + 1);
							break;
						case R.id.radio1:
							editor.putInt("c_2_n", n + 1);
							break;
						}
						break;
					case 3:
						y = mPrefs.getInt("c_3_y", 0);
						n = mPrefs.getInt("c_3_n", 0);
						switch (checkedId) {
						case R.id.radio0:
							editor.putInt("c_3_y", y + 1);
							break;
						case R.id.radio1:
							editor.putInt("c_3_n", n + 1);
							break;
						}
						break;
					case 4:
						y = mPrefs.getInt("c_4_y", 0);
						n = mPrefs.getInt("c_4_n", 0);
						switch (checkedId) {
						case R.id.radio0:
							editor.putInt("c_4_y", y + 1);
							break;
						case R.id.radio1:
							editor.putInt("c_4_n", n + 1);
							break;
						}
						break;
					}
					break;
				}
				editor.commit();
				Tot = y + n;
				a.setText("Y = " + y + "/" + Tot + "N = " + n + "/" + Tot);
			}
		});
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.question, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
