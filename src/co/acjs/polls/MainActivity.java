package co.acjs.polls;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

	public SharedPreferences mPrefs;
	public static SharedPreferences.Editor editor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mPrefs = getSharedPreferences("poll1", MODE_PRIVATE);
		mPrefs.getString("fname", "");
		mPrefs.getInt("age1", 0);
		String fnam = mPrefs.getString("fname", "");
		int age = mPrefs.getInt("age1", 0);
		if (fnam != "") {
			TextView namTV = (TextView) findViewById(R.id.nam);
			namTV.setText(fnam);
		}
		if (age != 0) {
			TextView ageTV = (TextView) findViewById(R.id.age);
			ageTV.setText(age+"");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void poll(View v) {
		editor = mPrefs.edit();
		TextView namTV = (TextView) findViewById(R.id.nam);
		TextView ageTV = (TextView) findViewById(R.id.age);
		editor.putString("fname", namTV.getText().toString());
		editor.putInt("age1", Integer.parseInt(ageTV.getText().toString()));
		editor.commit();
		Intent i = new Intent(this, PollActivity.class);
		startActivity(i);
	}
}
